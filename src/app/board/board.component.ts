import { Component, OnInit } from '@angular/core';

import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { ListService } from '../services/list.service';
import { List } from '../interfaces/list';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.less']
})
export class BoardComponent implements OnInit {

  lists: List[] = [];
  isShowModal: boolean = false;
  modalTitle: string = 'Add List';

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.lists, event.previousIndex, event.currentIndex);
  }

  constructor(
    private listService: ListService,
  ) {
  }

  ngOnInit(): void {
    this.getLists();
  }

  getLists(): void {
    this.listService.getLists().subscribe(lists => this.lists = lists);
  }

  showModal() {
      this.isShowModal = true;  
  }

  addColumn(title: string): void {
    if (!title) {
      return;
    }
    this.listService.addList({ title , cardCount: 0} as List).subscribe(
      list => {
        this.lists.push(list)
        console.log(this.lists)
      }
    );
  }

  closeModal(name: string): void {
    if(name) {
      this.addColumn(name);
    }
    
    this.isShowModal = false;
  }

}
