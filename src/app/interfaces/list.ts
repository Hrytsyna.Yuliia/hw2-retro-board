export interface List {
    id: number;
    title: string;
    cardCount: number;
}