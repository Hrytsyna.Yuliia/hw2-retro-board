export interface Card {
    id: number;
    title: string;
    comments: string[];
    votes: number;
    listId: number;
}