import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, Input, OnInit} from '@angular/core';
import { Card } from '../interfaces/card';
import { CardService } from '../services/card.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less']
})
export class ListComponent implements OnInit {

  @Input()
  title: string;

  @Input()
  id: string | number;

  isShowModal: boolean = false;
  modalTitle: string = "Add Card";

  cards: Card[] = [];

  constructor(
    private cardService: CardService
  ) { }

  ngOnInit(): void {
    this.getCards();
  }

  showModal() {
    this.isShowModal = true;
  }

  closeModal(title: string) {
    if (title) {
      this.addCard(title);
    }

    this.isShowModal = false;
  }

  getCards(): void {
    this.cardService.getCards().subscribe(
      cards => this.cards = cards.filter(card => card.listId === this.id)
    )

  }

  addCard(title: string): void {
    if (!title) {
      return;
    }
    this.cardService.addCard({ title, votes: 0, comments: [], listId: this.id} as Card).subscribe(
      card => this.cards.push(card)
    );

  }

  removeCard(id: number) {
    let card = this.cards.find(card => card.id === id);
    this.cards = this.cards.filter(card => card.id !== id);
    this.cardService.removeCard(card).subscribe();
  }

  dropCard(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      const newListId: number = +event.container.id;
      const cardId: number = +event.item.element.nativeElement.id;

      let cardObj: Card = this.cards.find(card => card.id === cardId);
      cardObj.listId = newListId;
      this.cardService.updateCard(cardObj).subscribe();
    }

  }

}

