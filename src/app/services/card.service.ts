import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Card } from '../interfaces/card';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  private cardUrl = 'api/cards';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  constructor(private http: HttpClient) { }

  addCard(card: Card): Observable<Card> {

    return this.http.post(this.cardUrl, card, this.httpOptions).pipe(
      tap((newCard: Card) => console.log(`added new Card ${newCard.title}`),
      catchError(this.handleError<Card>('can not add card')))
    );

  }

  getCards(): Observable<Card[]> {
    return this.http.get<Card[]>(this.cardUrl).pipe(
      catchError(this.handleError<Card[]>('can not get cards'))
    );
  }

  updateCard(card: Card): Observable<Card> {
    return this.http.put(this.cardUrl, card, this.httpOptions).pipe(
      tap((newCard: Card) => console.log(`changed`)),
      catchError(this.handleError<Card>('changed'))
    );
  }

  removeCard(card: Card): Observable<Card> {
    return this.http.delete<Card>(`${this.cardUrl}/${card.id}`, this.httpOptions).pipe(
      tap( _ => console.log(`deleted`)),
      catchError(this.handleError<Card>('delete'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T)
    }
  }
  
}
