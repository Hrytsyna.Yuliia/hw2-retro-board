import { Injectable } from '@angular/core';
import {InMemoryDbService} from 'angular-in-memory-web-api';

import { Card } from '../interfaces/card';
import { List } from '../interfaces/list';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  createDb() {
    // let lists: List[] = [{title: 'List1', id: 1, cardCount: 2}, {title: 'List2', id: 2, cardCount: 3}, {title: 'List3', id: 3, cardCount: 0}];
    // let cards: Card[] = [
    //   {id: 1, title: 'Card1', comments:[], votes: 0, listId: 1},
    //   {id: 2, title: 'Card2', comments:[], votes: 0, listId: 2},
    //   {id: 3, title: 'Card3', comments:[], votes: 7, listId: 2},
    //   {id: 4, title: 'Card4', comments:[], votes: 5, listId: 2},
    //   {id: 5, title: 'Card5', comments:[], votes: 0, listId: 1}
    // ];
    let lists: List[] = [];
    let cards: Card[] = [];
    return {lists, cards}
  }

  genId<T extends List | Card >(collection: T[]): number {
    return collection.length > 0 ? Math.max(...collection.map(item => item.id)) + 1 : 1;
  }
}
