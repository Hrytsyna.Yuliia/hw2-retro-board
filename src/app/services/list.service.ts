import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { List } from '../interfaces/list';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  private listsUrl = 'api/lists';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  constructor(
    private http: HttpClient
  ) { }

  getLists(): Observable<List[]> {
    return this.http.get<List[]>(this.listsUrl).pipe(
      catchError(this.handleError<List[]>('can not get lists'))
    );
  }

  addList(list: List): Observable<List> {
    return this.http.post(this.listsUrl, list, this.httpOptions).pipe(
      tap((newList: List) => console.log(`added new List ${newList.title}`),
      catchError(this.handleError<List>('can not add list')))
    );
    
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T)
    }
  }


}
