import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Card } from '../interfaces/card';
import { CardService } from '../services/card.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.less']
})
export class CardComponent implements OnInit {

  @Input()
  card: Card;

  title:string;
  votes: number;
  comments: string[];
  id:number;
  isShowModalCard: boolean = false;
  showComments: boolean = false;
  modalTitle:string;

  @Output()
  removeItem = new EventEmitter<number>();

  constructor(
    private cardService: CardService
  ) { }

  ngOnInit(): void {
    this.title = this.card.title;
    this.votes = this.card.votes;
    this.comments = this.card.comments;
    this.id = this.card.id;
  }

  ngDoCheck(): void {
    this.votes = this.card.votes;
    this.comments = this.card.comments;
  }

  increaseVotes(): void {
    this.card.votes = this.card.votes + 1;
    this.cardService.updateCard(this.card).subscribe()

  }

  showCommentsList(): void {
    this.showComments = !this.showComments;
  }

  addComment(): void {
    this.isShowModalCard = true;
    this.modalTitle = 'Add Comment';

  }

  removeComment(text: string): void {
    this.card.comments = this.comments.filter(item => item !== text);
    this.cardService.updateCard(this.card).subscribe(
      () => console.log(this.card.comments)
    );

  }

  removeCard(id: string): void {
    this.removeItem.emit(+id); 
  }



  closeModalCard(text: string): void {
    if(!text) {
      this.isShowModalCard = false;
      return;
    }
    this.card.comments.push(text);
    this.cardService.updateCard(this.card).subscribe();
    this.isShowModalCard = false;
    this.showCommentsList();
  }


}
