import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-add-button',
  templateUrl: './add-button.component.html',
  styleUrls: ['./add-button.component.less']
})
export class AddButtonComponent implements OnInit {

  @Input()
  id: string = '';

  @Output()
  open = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    
  }

  // getId(): void {
  //   this.id = 
  // }

  showModal(): void {
    this.open.emit();
  }

}
