import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.less']
})
export class ModalComponent implements OnInit {


  @Input()
  isShown = false;
  @Input()
  title: string = 'Modal window';

  @Output()
  close = new EventEmitter<string>();
  @Output()
  closeList = new EventEmitter<string>();

  name = '';

  constructor(
  ) { }

  ngOnInit(): void {
  }

  addItem(title: string): void {
    this.close.emit(title);
    
  }

  cancel(): void {
    this.close.emit(null);
  }

}
